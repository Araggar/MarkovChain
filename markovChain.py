from random import randint, choice
from os.path import isfile
from collections import defaultdict
from itertools import tee

# DEBUG METHODS

def debug_dict(d):
	for k, v in d.items():
		print("Key: {}".format(k))
		for w,n in v.items():
			print("\t{} : {}".format(w, n))

# DEBUG METHODS END

def pairwise(it):
	a, b = tee(it)
	next(b, None)
	return zip(a, b)

def weighted_random(conj):
	total_sum = randint(0, sum(conj.values()))
	keys = iter(conj)
	key = None
	while total_sum > 0:
		key = keys.__next__()
		total_sum += -conj[key]

	if key is None:
		key = keys.__next__()
	return key

def create_markov_dict(text):
	markov_dict = defaultdict(lambda : defaultdict(lambda : 0))
	if isfile(text):
		with open(text, 'r') as file:
			text = file.read().split()

	text = text.split if isinstance(text, str) else text

	for word, next_word in pairwise(text):
		markov_dict[word][next_word] += 1
	return markov_dict

def create_markov_chain(mk_dict, starting_words=None, length=True, stop_at_word=None, stop_at_symbol=None):
	word = choice(starting_words) if starting_words else choice(list(mk_dict.keys()))
	markov_chain = [word]
	while length:
		if not mk_dict[word]:
			break
		next_word = weighted_random(mk_dict[word])
		markov_chain.append(next_word)
		word = next_word
		length = length if isinstance(length, bool) else length-1
		if (next_word == stop_at_word or next_word[-1] == stop_at_symbol):
			break
	return markov_chain



if __name__ == "__main__":
	md = create_markov_dict('test.txt')
	debug_dict(md)
	mc = create_markov_chain(md,stop_at_symbol='.')
	print(" ".join(mc))

	